package space.outin.softwareEngineering.exercise1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("")
public class TodoController {

    @GetMapping("/hello")
    public String hello() {
        return "Hello Software Engineering CSCI4830-002";
    }
}
