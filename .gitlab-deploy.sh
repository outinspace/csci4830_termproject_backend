#!/bin/bash
set -f
echo "$SSH_PRIVATE_KEY" > aws.pem
chmod 600 aws.pem
mkdir ~/.ssh
ssh-keyscan $DEPLOY_SERVER >> ~/.ssh/known_hosts

mvn package

ssh -i aws.pem ubuntu@$DEPLOY_SERVER "sudo systemctl stop exercise1"

chmod 777 target/exercise1.jar

scp -i aws.pem target/exercise1.jar ubuntu@$DEPLOY_SERVER:/var/www/java
ssh -i aws.pem ubuntu@$DEPLOY_SERVER "sudo systemctl start exercise1"